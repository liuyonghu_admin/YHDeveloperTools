//
//  NSDictionary+overridObjectForKey.m
//  client
//
//  Created by victorLiu on 2018/1/25.
//  Copyright © 2018年 刘勇虎. All rights reserved.
//

#import "NSDictionary+overridObjectForKey.h"

@implementation NSDictionary (overridObjectForKey)
- (NSString *)checkedValueForkey:(NSString *)key{
//    NSLog(@"class = %@ ,key =%@",[[self objectForKey:key] class],key);
    return [[self objectForKey:key] isKindOfClass:[NSNull class]]?@"数据错误":[NSString stringWithFormat:@"%@",[self objectForKey:key]];
}
@end
