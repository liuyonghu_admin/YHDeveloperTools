//
//  NSString+MD5String.h
//  境外易购
//
//  Created by victorLiu on 2017/11/13.
//  Copyright © 2017年 刘勇虎. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSUInteger, YH_MD5Type) {
    YH_MD5_16         = 16,
    YH_MD5_32         = 32
};

@interface NSString (MD5String)
//加密
+ (NSString *)getMD5Str:(NSString *)str withType:(YH_MD5Type)type;
@end
