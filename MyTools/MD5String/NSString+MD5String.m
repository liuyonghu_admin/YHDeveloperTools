

//
//  NSString+MD5String.m
//  境外易购
//
//  Created by victorLiu on 2017/11/13.
//  Copyright © 2017年 刘勇虎. All rights reserved.
//

#import "NSString+MD5String.h"
#import <CommonCrypto/CommonDigest.h>
@implementation NSString (MD5String)
+ (NSString *)getMD5Str:(NSString *)str withType:(YH_MD5Type)type{
    const char *cStr = [str UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), result );
    NSMutableString *tempStr  = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH ];
    for (NSInteger i = 0; i < CC_MD5_DIGEST_LENGTH ; i ++) {
//        转大写
        [tempStr appendFormat:@"%02X",result[i]];
    }
    
    NSString * resultStr;
    switch (type) {
        case YH_MD5_16:{
            resultStr   = [tempStr substringWithRange:NSMakeRange(8, YH_MD5_16)];
        }
            break;
        case YH_MD5_32:{
            resultStr = tempStr;
        }
            break;
        default:
            break;
    }
    
//    NSLog(@"result length = %d",sizeof(result));
    return resultStr;
}
@end
