//
//  TipsVIew.h
//  client
//
//  Created by victorLiu on 2018/1/22.
//  Copyright © 2018年 刘勇虎. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger,TypeRender){
    noOrderData = 0
};
@interface TipsView : UIView
@property(assign,nonatomic)CGFloat imageToTop;
@property(assign,nonatomic)TypeRender tipsType;

@end
