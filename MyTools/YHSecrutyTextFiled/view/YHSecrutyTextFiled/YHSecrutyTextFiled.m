//
//  SecrutyTextFiled.m
//  NewKroreaCards
//
//  Created by victorLiu on 2017/5/23.
//  Copyright © 2017年  刘勇虎. All rights reserved.
//

#import "YHSecrutyTextFiled.h"
#import "UIColor+GetRGBWithHexadecimalColor.h"


@interface YHSecrutyTextFiled ()<UITextFieldDelegate>
@property(nonatomic,strong)NSMutableArray *labelArrary;
@property(nonatomic,assign)NSInteger flag;
@property(nonatomic,strong)NSMutableString *value;
@property(nonatomic,strong)UITextField *textfield;
@end

@implementation YHSecrutyTextFiled

-(instancetype)initWithFrame:(CGRect)frame{
    if (self == [super initWithFrame:frame]) {
        _flag = 0;
        _textfield = [[UITextField alloc]initWithFrame:CGRectZero];
        [_textfield setKeyboardType:UIKeyboardTypeNumberPad];
        [self addSubview:_textfield];
        [_textfield becomeFirstResponder];
        _textfield.hidden = YES;
        _textfield.delegate =self;
        
        for (NSInteger i = 0; i <6; i++) {
            UIImageView *label = [[UIImageView alloc]initWithFrame:CGRectMake((frame.size.width/6)*i + 10, 10, frame.size.width/6-20, frame.size.height-20)];
//            label.layer.borderColor = [UIColor getRGBWithHexadecimalColor:homePageBackgroundColor].CGColor;
            label.layer.borderWidth = 2;
            label.layer.masksToBounds = YES;
            label.layer.cornerRadius = 5;
            label.userInteractionEnabled = YES;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(maekBecomeFirstResponder)];
            [label addGestureRecognizer:tap];
            [self.labelArrary addObject:label];
            [self addSubview:label];
        }
    }
    return self;
}
- (void)maekBecomeFirstResponder{
    [_textfield becomeFirstResponder];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (string.length > 0) {
        if (_flag < 6) {
            _flag ++;
            self.value =  [[self.value stringByAppendingString:string] mutableCopy];
        }
    }else{
        _flag --;
        [self.value replaceCharactersInRange:NSMakeRange(_value.length -1, 1) withString:@""];
    }
    
    for (NSInteger i = 0; i <_labelArrary.count; i++) {
        UIImageView *label = _labelArrary[i];
        if (i < _flag) {
            label.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"圆点" ofType:@"png"]];
        }else{
            label.image =nil;
        }
    }
    if (_flag == 6) {
        if (self.valueConfrim != nil) {
           self.valueConfrim(_value);  
        }
       
    }
//    NSLog(@"string = %@ --%ld",self.value,(long)_flag);
    if (_flag < 7) {
        return YES;
    }
    _flag = 6;
    return NO;
}

-(NSMutableArray *)labelArrary{
    if (!_labelArrary) {
        _labelArrary = [[NSMutableArray alloc]init];
    }
    return _labelArrary;
}

-(NSMutableString *)value{
    if (!_value) {
        _value = [[NSMutableString alloc]init];
    }
    return _value;
}
- (void)resetYHSecurtyTextfiled{
    _flag = 0;
    _textfield.text = @"";
    _value = [[NSMutableString alloc]init];
    for (NSInteger i = 0; i <_labelArrary.count; i++) {
        UIImageView *label = _labelArrary[i];
        if (i < _flag) {
            label.image = [[UIImage alloc]init];
        }else{
            label.image =nil;
        }
    }
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
