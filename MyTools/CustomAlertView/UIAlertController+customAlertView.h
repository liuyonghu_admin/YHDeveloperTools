//
//  UIAlertController+customAlertView.h
//  境外易购
//
//  Created by victorLiu on 2017/11/10.
//  Copyright © 2017年 刘勇虎. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController (customAlertView)
+ (void)showTipsAlertWithTitle:(NSString *_Nullable)title subTitle:(NSString *_Nullable)subTitle  cAction:(nullable SEL)cAction userinfo:(NSDictionary *_Nullable)userinfo hasCancel:(BOOL)hasCancel addTarget:(UIViewController *_Nullable)target;
@end
