//
//  NSString+GetSizeOfString.m
//  NewKroreaCards
//
//  Created by victorLiu on 2017/6/1.
//  Copyright © 2017年  刘勇虎. All rights reserved.
//

#import "NSString+GetSizeOfString.h"

@implementation NSString (GetSizeOfString)
+ (CGFloat)getWidthWithHeight:(CGFloat)height string:(NSString *)string fontSizeAttrubute:(NSDictionary *)attr{
   CGSize size = CGSizeMake(10000, height);
    if (!attr) {
        attr = @{NSFontAttributeName:[UIFont systemFontOfSize:20 weight:1]};
    }
    return [string boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:attr context:nil].size.width;
}

+ (CGFloat)getHeightWithWidth:(CGFloat)width string:(NSString *)string fontSizeAttrubute:(NSDictionary *)attr{
    CGSize size = CGSizeMake(width, 10000);
    if (!attr) {
        attr = @{NSFontAttributeName:[UIFont systemFontOfSize:20 weight:1]};
    }
 
    return [string boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:attr context:nil].size.height;
}
@end
