//
//  NSString+GetSizeOfString.h
//  NewKroreaCards
//
//  Created by victorLiu on 2017/6/1.
//  Copyright © 2017年  刘勇虎. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface NSString (GetSizeOfString)
+ (CGFloat)getWidthWithHeight:(CGFloat)height string:(NSString *)string fontSizeAttrubute:(NSDictionary *)attr;

+ (CGFloat)getHeightWithWidth:(CGFloat)width string:(NSString *)string fontSizeAttrubute:(NSDictionary *)attr;
@end
