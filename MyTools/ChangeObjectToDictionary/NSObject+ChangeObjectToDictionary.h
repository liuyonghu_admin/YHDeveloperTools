//
//  NSObject+ChangeObjectToDictionary.h
//  NewKroreaCards
//
//  Created by victorLiu on 2017/12/7.
//  Copyright © 2017年  刘勇虎. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (ChangeObjectToDictionary)
- (NSDictionary *)changeToDictionaryWithObject:(NSObject *)object;
@end
