//
//  UIView+RemoveAllSubView.h
//  client
//
//  Created by victorLiu on 2018/1/24.
//  Copyright © 2018年 刘勇虎. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (RemoveAllSubView)
- (void)removeAllSubView;
@end
