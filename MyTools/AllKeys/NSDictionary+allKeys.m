//
//  NSDictionary+allKeys.m
//  境外易购
//
//  Created by victorLiu on 2017/11/13.
//  Copyright © 2017年 刘勇虎. All rights reserved.
//

#import "NSDictionary+allKeys.h"

@implementation NSDictionary (allKeys)
static NSMutableArray *keysArr;
+ (NSMutableArray *)YHAllKeysWithDictionary:(NSDictionary *)dictionary{
    
    keysArr = [[NSMutableArray alloc]init];
    
    [NSDictionary checkDic:dictionary];
    return keysArr;
}

+ (void)checkDic:(NSDictionary *)dic{
    keysArr =  [[keysArr arrayByAddingObjectsFromArray:dic.allKeys] mutableCopy];
    for (id value  in dic.allValues) {
        if ([value isKindOfClass:[NSDictionary class]]) {
            [NSDictionary checkDic:value];
        }
    }
}

+ (NSMutableArray *)YHValueArraryWithDictionary:(NSDictionary *)dictionary{
    NSMutableArray *tempArr = [NSMutableArray array];
    NSMutableDictionary *headerDic = [[dictionary objectForKey:@"packetHead"] mutableCopy];
    NSDictionary *bodyDic = [dictionary objectForKey:@"packetBody"];
   [headerDic removeObjectForKey:@"sign"];
    
    NSArray *headerKeys =headerDic.allKeys;
    NSArray *bodyKeys =bodyDic.allKeys;
    NSMutableArray *keysArr = [NSMutableArray arrayWithArray:headerKeys];
    [keysArr addObjectsFromArray:bodyKeys];
    keysArr = [[keysArr sortedArrayUsingSelector:@selector(compare:)] copy];
  
    for (NSString *key in keysArr) {
        id value =  ([headerDic valueForKey:key] != nil)?[headerDic valueForKey:key]:[bodyDic valueForKey:key];;
        NSString *tempStr;
//        数据对象转化
        if ([value isKindOfClass:[NSArray class]]) {
            NSLog(@"arrary == %@",value);
            NSData *tempData = [NSJSONSerialization dataWithJSONObject:value options:0 error:nil];
            value = [[NSString alloc]initWithData:tempData encoding:NSUTF8StringEncoding];
        }
        //MARK:        字符串为空时跳过
        if (!value || ([value isKindOfClass:[NSString class]] && [value isEqualToString:@""])) {
            continue;
        }
       
        NSMutableString *tempString = [[NSMutableString alloc]initWithString:value];
        for (NSInteger i = 0; i < tempString.length; i++) {
            NSString *character = [tempString substringWithRange:NSMakeRange(i, 1)];
            if ([character isEqualToString:@"\\"]) {
                [tempString deleteCharactersInRange:NSMakeRange(i, 1)];
            }
//            if ([character isEqualToString:@" "]) {
//                [tempString deleteCharactersInRange:NSMakeRange(i, 1)];
//            }
        }
        tempStr = [NSString stringWithFormat:@"%@=%@",key,tempString];
        
        [tempArr addObject:tempStr];
    }
    
    return tempArr;
}

+ (NSString  *)YHSortedStringWithDictionary:(NSDictionary *)dictionary{
    NSArray *tempArr  =  [self YHValueArraryWithDictionary:dictionary] ;
    NSMutableString *resultStr  = [NSMutableString string];
    
    for (NSInteger i = 0; i < tempArr.count; i ++) {
       
            if (i) {
                [resultStr appendFormat:@"&%@",tempArr[i]];
            }
            else{
                [resultStr appendFormat:@"%@",tempArr[i]];
            }
    }
    //    sign=FHGHGFHFGHFGHGFHGFHGFHFGHF
//    [resultStr appendFormat:@"&sign=%@",NetSignKEY];
//
    return resultStr;
}


@end
