//
//  NSObject+ParametersInitMethod.m
//  client
//
//  Created by victorLiu on 2018/2/2.
//  Copyright © 2018年 刘勇虎. All rights reserved.
//

#import <objc/runtime.h>
#import "NSObject+ParametersInitMethod.h"

@implementation NSObject (ParametersInitMethod)
-(void)YHDataInit{
    unsigned int propertyCount = 0;
    objc_property_t *propertys = class_copyPropertyList([self class], &propertyCount);
    for (NSInteger i = 0; i < propertyCount; i ++) {
        objc_property_t property = propertys[i];
        const char *propertyName = property_getName(property);
        [self setValue:nil forKey: [NSString stringWithUTF8String:propertyName]];
    }
    free(propertys);

}
@end
