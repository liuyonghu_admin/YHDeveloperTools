//
//  NSObject+ParametersInitMethod.h
//  client
//
//  Created by victorLiu on 2018/2/2.
//  Copyright © 2018年 刘勇虎. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (ParametersInitMethod)
- (void)YHDataInit;
@end
