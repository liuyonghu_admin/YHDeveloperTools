//
//  Paramters.h
//  client
//
//  Created by victorLiu on 2018/1/17.
//  Copyright © 2018年 刘勇虎. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Paramters : NSObject

//token
@property(strong,nonatomic)NSString *token;
//page 分页参数
@property(strong,nonatomic)NSNumber *page;
//订单标识0-所有订单；1-未完成订单
@property(strong,nonatomic)NSString *orderType;
//每页显示条数（默认为10）
@property(strong,nonatomic)NSNumber *size;
@property(strong,nonatomic)NSString *size1;
@property(strong,nonatomic)NSString *keyForId;
//消息类型 order:订单消息
@property(strong,nonatomic)NSString *fMsgType;

//消息ID： 若传-1：全部已读
@property(strong,nonatomic)NSString *msgId;

//账户ID
@property(strong,nonatomic)NSString *fIndentNo;
//验证码
@property(strong,nonatomic)NSString *fMsgCode;
//发送场景 dirverLogin:司机端登录 misLogin:客户端登录
@property(strong,nonatomic)NSString *smsType;
//手机号
@property(strong,nonatomic)NSString *phone;

@end
