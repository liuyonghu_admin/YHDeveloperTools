//
//  UIImage+GetQRCodeWithStr.h
//  client
//
//  Created by victorLiu on 2018/1/25.
//  Copyright © 2018年 刘勇虎. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (GetQRCodeWithStr)
+ (UIImage *)getQRCodeWithString:(NSString *)str withSize:(CGFloat)size;
@end
