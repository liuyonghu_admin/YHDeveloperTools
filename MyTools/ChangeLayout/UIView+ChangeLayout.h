//
//  UIView+ChangeLayout.h
//  zhangshao
//
//  Created by victorLiu on 2018/3/16.
//  Copyright © 2018年 刘勇虎. All rights reserved.
//





#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>



@interface UIView (ChangeLayout)
- (void)changeLayoutWithType:(NSLayoutAttribute)layoutName constant:(CGFloat)constant;
@end
