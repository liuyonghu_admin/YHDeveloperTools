//
//  AppUIParameterHeader.h
//  client
//
//  Created by victorLiu on 2018/1/15.
//  Copyright © 2018年 刘勇虎. All rights reserved.
//

#ifndef AppUIParameterHeader_h
#define AppUIParameterHeader_h

#define orderListCellTipsColor @"#FFFF882C"
#define orderListCellDefaultColor @"#fafafa"
#define homePageBackgroundColor @"#f7f7f7"
/**/
#define orderListButtonTitleColor @"#15B4F1"
/**/
#define sWidth               [UIScreen mainScreen].bounds.size.width
#define sHeight              [UIScreen mainScreen].bounds.size.height
#define homePageListCellSectionHeight  40


#define navigationLeftbtnWidth   44
#define navigationLeftbtnHight   44
#endif /* AppUIParameterHeader_h */


